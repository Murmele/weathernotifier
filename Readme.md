# Weather notifier
Application to receive all weather data on your smartphone over the matrix client

Supported data:
- Austrocontrol (https://www.austrocontrol.at/)
- Pressure gradient Bozen - Innsbruck (https://wetter.provinz.bz.it/foehndiagramm.asp)
- Profiwetter.ch (https://profiwetter.ch)
- Deutscher Wetterdienst (https://www.dwd.de)
- DHV (https://www.dhv.de/piloteninfos/wetter/)

# Installation instructions
## Matrix Instance
Matrix is used as communication channel. You need a matrix account for the weather notifier and one personal one to which the notifier will send the data
1) Create matrix client for your notifier at https://element.io/
2) Create password file for the matrix client: "matrix_username_password.txt"
  - First line: username
  - Second line: password
  
## Austrocontrol
1) Create password file for austrocontrol: "austrocontrol_username_password.txt"
  - First line: username
  - Second line: password

# Usage

1) Download this repository
2) Go into repository folder
3) Install poetry: https://python-poetry.org/docs/
4) Install dependencies `poetry install`
5) Execute script: `poetry run python weathernotifier/main.py`

# Install linux daemon to automatically start the script at startup
source: https://stackoverflow.com/questions/42735934/running-python-script-as-a-systemd-service
1) execute `service/install.sh` script
2) enable service: `systemctl enable weathernotifier`
3) restart or start service manually: `systemctl start weathernotifier`

## Verifying if running
`systemctl --type=service`
`systemctl status weathernotifier`

## Troubleshooting
1) `start-stop-daemon: Kommando nicht gefunden`: install start-stop-daemon

