# Data from dwd.de
# Deutscher Wetter Dienst

# https://www.windinfo.eu/wettervorhersage/isobarenkarte-europa/
import datetime
import common
import os.path


class DWDException(Exception):
    pass


# TODO: check which is correct, V or 0
# ico_tkboden_na_036_url = "https://www.dwd.de/DWD/wetter/wv_spez/hobbymet/wetterkarten/ico_tkboden_na_036.png"
# ico_tkboden_na_V36_url = "https://www.dwd.de/DWD/wetter/wv_spez/hobbymet/wetterkarten/ico_tkboden_na_V36.png"

forecast_hours = [24, 36, 48, 60, 84, 108]


def url(forecast_hour): return f"https://www.dwd.de/DWD/wetter/wv_spez/hobbymet/wetterkarten/ico_tkboden_na_{forecast_hour:03d}.png"


def fetch_all(export_path: str) -> None:
    for forecast_hour in forecast_hours:
        fetch_ico_tkboden(export_path, forecast_hour)


def create_filename(date: datetime.datetime, forecast_hour: int):
    if forecast_hour not in forecast_hours:
        raise DWDException("Invalid forecast number")
    return common.create_filename(date, "dwd", f"isobaren_{forecast_hour}", "png")


def fetch_ico_tkboden(export_path, forecast_hour):
    if forecast_hour not in forecast_hours:
        raise DWDException("Invalid forecast number")
    filename = create_filename(datetime.datetime.utcnow(), forecast_hour)
    filename = os.path.join(export_path, filename)
    if not os.path.isfile(filename):
        common.fetch_image(url(forecast_hour), filename)

    return filename


if __name__ == "__main__":
    fetch_ico_tkboden("export", 24)
