import asyncio
from nio import AsyncClient
from matrix import send_markdown, send_image, send_text, send_code
import os.path
import datetime
import time
import pathlib

# weather pages
import dwd
import profiwetter_ch
import suedtirol_wetter
import dhv
import austrocontrol

# async def message_callback(room: MatrixRoom, event: RoomMessageText) -> None:
#     print(
#         f"Message received in room {room.display_name}\n"
#         f"{room.user_name(event.sender)} | {event.body}"
#     )
server = "matrix.org"

async def main() -> None:
    file_path = pathlib.Path(__file__).parent.absolute()
    matrix_credentials = os.path.join(file_path, "../matrix_username_password.txt")
    if os.path.isfile(matrix_credentials):
        with open(matrix_credentials, "r") as f:
            username = f.readline().strip()
            password = f.readline().strip()
    else:
        raise Exception("No matrix account found.")


    client = AsyncClient(f"https://{server}", f"{username}")
    #client.add_event_callback(message_callback, RoomMessageText)

    export_path = "export"

    print(await client.login(password))

    room_ids = []
    send_notification = True
    send_initial = True

    ac = None
    austrocontrol_credentials = os.path.join(file_path, "../austrocontrol_username_password.txt")
    if os.path.isfile(austrocontrol_credentials):
        with open(austrocontrol_credentials, "r") as f:
            username = f.readline().strip()
            password = f.readline().strip()
        ac = austrocontrol.Austrocontrol(username, password)



    while True:
        sync_response = await client.sync(timeout=3000)  # milliseconds

        t = datetime.datetime.now().time()
        if datetime.time(00, 00, 00) < t < datetime.time(1, 00, 00):  # reset at midnight
            time.sleep(1)  # to be sure it will not be cleared a second time
            send_notification = True

        if len(sync_response.rooms.join) > 0:
            joins = sync_response.rooms.join
            for room_id in joins:
                # for event in joins[room_id].timeline.events:
                #     print(event)
                if room_id not in room_ids:
                    room_ids.append(room_id)

                # TODO: check if I joined new rooms

        if (send_notification is True and t > datetime.time(6, 00, 00)) or send_initial:  # send report at 6 o clock in the morning
            send_initial = False
            send_notification = False
            for room_id in room_ids:
                await send_image(client, room_id, suedtirol_wetter.fetch_gradient(export_path))
                await send_image(client, room_id, dwd.fetch_ico_tkboden(export_path, 24))
                await send_image(client, room_id, dwd.fetch_ico_tkboden(export_path, 36))
                await send_image(client, room_id, dwd.fetch_ico_tkboden(export_path, 48))
                await send_image(client, room_id, dwd.fetch_ico_tkboden(export_path, 60))
                await send_image(client, room_id, dwd.fetch_ico_tkboden(export_path, 84))

                await send_image(client, room_id, ac.fetch_overview(austrocontrol.Day.Today, austrocontrol.ChartType.ThermalQuality, export_path))

                # North Alps
                weather = dhv.fetchWeather(export_path)
                message = "# North Alps\n"
                for w in weather["north_alps"]:
                    message += dhv.weather_to_markdown(w) + "\n"
                await send_markdown(client, room_id, message)

                await send_image(client, room_id, ac.fetch_detail(austrocontrol.Day.Today, austrocontrol.Region.OestlicheBayrischeAlpen, export_path))
                await send_code(client, room_id, ac.fetch_diagram(austrocontrol.Day.Today, austrocontrol.Region.OestlicheBayrischeAlpen, export_path))
                await send_image(client, room_id, ac.fetch_detail(austrocontrol.Day.Tomorrow, austrocontrol.Region.OestlicheBayrischeAlpen, export_path))
                await send_code(client, room_id, ac.fetch_diagram(austrocontrol.Day.Tomorrow, austrocontrol.Region.OestlicheBayrischeAlpen, export_path))

                # South Alps
                weather = dhv.fetchWeather(export_path)
                message = "# South Alps\n"
                for w in weather["south_alps"]:
                    message += dhv.weather_to_markdown(w) + "\n"
                await send_markdown(client, room_id, message)
                await send_image(client, room_id, profiwetter_ch.fetch(export_path, profiwetter_ch.Region.Meran))
                await send_image(client, room_id, profiwetter_ch.fetch(export_path, profiwetter_ch.Region.Brauneck))
                await send_image(client, room_id, ac.fetch_temp(austrocontrol.RegionTemp.Innsbruck, export_path))
                await send_image(client, room_id, ac.fetch_detail(austrocontrol.Day.Today, austrocontrol.Region.Vinschgau_Pustertal, export_path))
                await send_code(client, room_id, ac.fetch_diagram(austrocontrol.Day.Today, austrocontrol.Region.Vinschgau_Pustertal, export_path))
                await send_image(client, room_id, ac.fetch_detail(austrocontrol.Day.Tomorrow, austrocontrol.Region.Vinschgau_Pustertal,export_path))
                await send_code(client, room_id, ac.fetch_diagram(austrocontrol.Day.Tomorrow, austrocontrol.Region.Vinschgau_Pustertal,export_path))


asyncio.get_event_loop().run_until_complete(main())
