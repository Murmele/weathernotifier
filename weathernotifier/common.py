import os
import requests  # to fetch data
import datetime


def save_image(raw_image, filename):
    (path, _) = os.path.split(filename)
    os.makedirs(path, exist_ok=True)
    with open(filename, 'wb') as out_file:
        out_file.write(raw_image)


def format_datetime():
    return "%G-%m-%d"


def create_filename(datetime_chart: datetime.datetime, provider: str, description: str, fileending: str):
    # datetime of the forecast, datetime fetched, provider, description
    # 2022-07-12_2022-07-11_austrocontrol_thermal_quality
    filename = datetime_chart.strftime(format_datetime())  # datetime of the forecast
    filename += "_" + datetime.datetime.utcnow().strftime(format_datetime())  # datetime fetched
    filename += "_" + provider  # provider
    if description:
        filename += "_" + description  # description
    if not fileending.startswith("."):
        filename += "."
    filename += fileending

    return filename


def fetch_image(url, export_filename):
    """
    Fetches an image from the internet and stores it localy
    :param url:
    """
    response = requests.get(url, stream=True)

    if response.reason != "OK":
        return "", ""

    save_image(response.content, export_filename)

    split = url.rsplit('.', 1)
    if len(split) < 2:
        return "", ""

    filetype = split[1]

    return filetype
