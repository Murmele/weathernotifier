import datetime
import common
import os.path

url = "https://wetter.provinz.bz.it/images/pgradient_de.png"


def create_filename(date: datetime.datetime):
    return common.create_filename(date, "provinz_bz", "foehndiagramm", "png")


def fetch_gradient(export_path):
    filename = create_filename(datetime.datetime.utcnow())
    filename = os.path.join(export_path, filename)
    if not os.path.isfile(filename):
        common.fetch_image(url, filename)

    return filename


if __name__ == "__main__":
    fetch_gradient("export")
