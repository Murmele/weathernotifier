# www.profiwetter.ch
import enum
from posixpath import join as urljoin
import datetime
import common
import os.path

base_url = "https://profiwetter.ch"

# example: meran: https://profiwetter.ch/mos_P0011.svg


# All regions with PXXXX are old and inaccurate for wind speed in high heights
class Region(enum.Enum):
    Meran = "P0011"
    Brauneck = "Z929"
    Wank = "P0306"  # wind really inaccurate

def fetch_all(export_path):
    for region in Region:
        fetch(export_path, region)


def create_url(region: Region):
    return urljoin(base_url, f"mos_{region.value}.svg")


def create_filename(date: datetime.datetime, region: Region):
    return common.create_filename(date, "profiwetter", region.name, "svg")


def fetch(export_path, region: Region):
    filename = create_filename(datetime.datetime.utcnow(), region)
    filename = os.path.join(export_path, filename)
    if not os.path.isfile(filename):
        common.fetch_image(create_url(region), filename)

    return filename


if __name__ == "__main__":
    fetch_all("export")
    fetch("export", Region.Meran)

