# Found in the page html: <form name="login" method="post" action="acglogin.cgi" id="login-form" class="form-horizontal">
import enum
import requests
from posixpath import join as urljoin
import os.path
import datetime
import common
import time


class AustroControlError(Exception):
    pass


class Region(enum.Enum):
    Oetztaler_Alpen = 66
    Oberdrautal_Gailtal = 67
    BuckligeWelt_Vorarlberg = 70
    Muehlviertel = 77
    Salzkammergut_Tennengau = 79
    Weinviertel_Wienerwald = 80
    OestlicheBayrischeAlpen = 84
    Waldviertel = 85
    ZentralraumOOE_NOE = 86
    WienerBecken_Nordburgenland = 87
    GlarnerAlpen_BregenzerWald = 88
    Unterinntal_Steinberge = 89
    Salzachtal_Oberennstal = 91
    ZillertalerAlpen = 93
    Murtal_Muerztal = 94
    SteirischesRandgebirge = 96
    Suedsteiermark_Suedburgenland = 97
    Engadin = 105
    Graubuenden_Montafon = 107
    Vinschgau_Pustertal = 118


class RegionTemp(enum.Enum):
    Innsbruck = 11120
    Muenchen = 10868
    Altenstadt = 10954


class Day(enum.Enum):
    Yesterday = "Gestern"
    Today = "Bilder"
    Tomorrow = "Morgen"


class ChartType(enum.Enum):
    ThermalQuality = "thst"  # Thermikqualität/Steigwerte
    OperationalHeight = "opau"  # Operationshöhe/Thermikauslöse
    SlopeWind = "hawi"  # Hangwind

login_page = "https://www.austrocontrol.at/flugwetter/index.php"
alptherm_url = "https://www.austrocontrol.at/flugwetter/products/alptherm"
temps_url = "https://www.austrocontrol.at/flugwetter/products/TEMPS"


def dayToDateTime(day: Day):
    date = datetime.datetime.utcnow()
    if day == Day.Yesterday:
        date -= datetime.timedelta(1)  # -24h
    elif day == Day.Tomorrow:
        date += datetime.timedelta(1)  # +24h
    return date


class Austrocontrol():
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.session = None
        self.timeout_s = 5  # timeout of the session [s]
        self.max = 2
        self.authentication_time = 0
        self.authenticate()

    def authenticate(self):
        if self.session is not None:
            self.session.close()

        self.session = requests.Session()
        self.session.headers.update({'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64)"})

        payload = {'httpd_username': self.username,
                   'httpd_password': self.password,
                   'httpd_body': 'http://www.austrocontrol.at/flugwetter/index.php',
                   'lang': 'de',
                   'back': 'http://www.austrocontrol.at/flugwetter/index.php',
                   'login': 'login'}
        response = self.session.post(login_page, data=payload)  # do login
        if not response.ok:
            self.session.close()
            self.session = None
            print("Austrocontrol Warning: login was not successful")
            return
        if "Warning" in response.headers:
            if response.headers["Warning"] == "Login":
                raise AustroControlError("Login to Austrocontrol failed.")
            else:
                raise AustroControlError(f"Unable to login: {response.headers['Warning']}")

        if "Keep-Alive" in response.headers:
            keep_alive = response.headers["Keep-Alive"].split(",")
            if len(keep_alive) > 1:
                timeout_l = keep_alive[0].strip().split("=")
                self.timeout_s= 2  # default value (quite common)
                if len(timeout_l) > 1:
                    self.timeout_s = int(timeout_l[1])
                max_l = keep_alive[1].strip().split("=")
                self.max = 1
                if len(max_l) > 1:
                    self.max = int(max_l[1])
        self.authentication_time = time.time()

    def get(self, url):
        if time.time() - self.authentication_time > self.timeout_s or self.session is None:
            if not self.authenticate():  # renew connection because of timeout it was closed
                return None
        return self.session.get(url, stream=True)

    def fetch_all(self, export_folder: str) -> None:

        # fetch overview
        for chart in ChartType:
            for day in Day:
                self.fetch_overview(day, chart, export_folder)

        # fetch detail view
        # fetch diagram
        for region in Region:
            for day in Day:
                self.fetch_detail(day, region, export_folder)
                self.fetch_diagram(day, region, export_folder)

        # fetch temps
        for region in RegionTemp:
            self.fetch_temp(region, export_folder)

    def fetch_overview(self, day: Day, chart_type: ChartType, export_folder):
        url = urljoin(alptherm_url, day.value, f"os_chart_{chart_type.value}.jpg")
        date = dayToDateTime(day)

        filename = common.create_filename(date, "austrocontrol", chart_type.value, ".jpg")
        export_file = os.path.join(export_folder, filename)

        if not os.path.isfile(export_file):
            res = self.get(url)
            if res is None:
                return ""


            if not res.ok:
                raise AustroControlError("Unable to download file: " + filename + f": {res.reason}")

            common.save_image(res.content, export_file)

        return export_file

    # https://www.austrocontrol.at/flugwetter/products/alptherm/Gestern/gg088.png
    def fetch_detail(self, day: Day, region: Region, export_folder):
        date = dayToDateTime(day)
        url = urljoin(alptherm_url, day.value, f"gg{region.value:03d}.png")

        filename = common.create_filename(date, "austrocontrol", region.name, ".png")
        export_file = os.path.join(export_folder, filename)

        if not os.path.isfile(export_file):
            res = self.get(url)
            if res is None:
                return ""
            if not res.ok:
                raise AustroControlError("Unable to download file: " + filename + f": {res.reason}")

            common.save_image(res.content, export_file)

        return export_file

    def fetch_diagram(self, day: Day, region: Region, export_folder):
        date = dayToDateTime(day)
        url = urljoin(alptherm_url, day.value, f"GGGG{region.value}")

        filename = common.create_filename(date, "austrocontrol", region.name, ".txt")
        export_file = os.path.join(export_folder, filename)

        if not os.path.isfile(export_file):
            res = self.get(url)
            if res is None:
                return ""
            text = res.text

            if not res.ok:
                raise AustroControlError("Unable to download file: " + filename + f": {res.reason}")

            with open(export_file, "w") as f:
                f.write(text)
        else:
            with open(export_file, "r") as f:
                text = f.read()

        return text

    def fetch_temp(self, region: RegionTemp, export_folder):
        # url = https://www.austrocontrol.at/flugwetter/products/TEMPS/TEMP11120.gif?mtime=1657768571
        date = dayToDateTime(Day.Today)
        url = urljoin(temps_url, f"TEMP{region.value}.gif")

        filename = common.create_filename(date, "austrocontrol", region.name, ".gif")
        export_file = os.path.join(export_folder, filename)

        if not os.path.isfile(export_file):
            res = self.get(url)
            if res is None:
                return ""

            if not res.ok:
                raise AustroControlError("Unable to download file: " + filename + f": {res.reason}")

            common.save_image(res.content, export_file)

        return export_file


if __name__ == "__main__":
    if os.path.isfile("austrocontrol_username_password.txt"):
        with open("austrocontrol_username_password.txt", "r") as f:
            username = f.readline().strip()
            password = f.readline().strip()
        ac = Austrocontrol(username, password)
        ac.fetch_detail(Day.Today, Region.OestlicheBayrischeAlpen, "export")
        ac.fetch_overview(Day.Yesterday, ChartType.ThermalQuality, "export")
        ac.fetch_diagram(Day.Today, Region.OestlicheBayrischeAlpen, "export")
        ac.fetch_temp(RegionTemp.Innsbruck, "export")
