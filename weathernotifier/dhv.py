import datetime
import bs4.element
import requests
from bs4 import BeautifulSoup
import common
import os.path
import xml.etree.ElementTree as ET
from xml.dom import minidom

url = "https://www.dhv.de/piloteninfos/wetter/"

class Weather():
    def __init__(self, title, color, information):
        self.title = title
        self.color = color
        self.information = information
        self.date = None
        self.weekday = None

    def __repr__(self):
        return f"\nDate: {self.date} Title: {self.title}\nDescription: {self.information}\n"

    def setDate(self, date: datetime.datetime):
        self.date = date

def weather_to_markdown(weather: Weather):
    text = ""
    text += f"## {weather.weekday} {weather.date.strftime('%d.%m.%Y')} {weather.title}\n"
    for info in weather.information:
        text += info + "\n"
    return text

def parse_weather_tag(tag: bs4.element.Tag):
    color = tag.attrs["class"][0]
    title = tag.find('h1').text
    information = []

    for child in tag:
        if type(child) is bs4.element.Tag and child.name == "p":
            text = ""
            for p in child.contents:
                t = p.text
                if len(t) > 0 and not t[0].isprintable():
                    t = t[1:]
                if len(t) > 0 and not t[-1].isprintable():
                    t = t[0:-1]
                text += t
            information.append(text)

    return Weather(title, color, information)

def german_weekday_to_iso(day: str):
    german = ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"]
    return german.index(day) + 1  # ISO weekday starts from 1

def create_filename(date: datetime.datetime):
    return common.create_filename(date, "dhv", "wetter", "xml")

def readXML(filename: str) -> []:
    weather = {
        "germany": [],
        "north_alps": [],
        "south_alps": []
    }
    tree = ET.parse(filename)
    root = tree.getroot()
    if root.tag != "weather":
        return weather

    for region in root:
        if region.tag not in weather:
            continue

        for day in region:
            if day.tag != "weather":
                continue
            title = day.attrib["title"]
            color = day.attrib["color"]
            information = []
            for info in day:
                if info.tag != "info":
                    continue
                information.append(info.text)
            d = Weather(title, color, information)
            d.weekday = day.attrib["weekday"]
            d.date = datetime.datetime.strptime(day.attrib["date"], "%Y-%m-%d %H:%M:%S")
            weather[region.tag].append(d)

    return weather

def writeXML(weather: [], export_path, filename: str) -> None:
    # store offline
    os.makedirs(export_path, exist_ok=True)
    root = ET.Element('weather', {})
    for region in weather:
        r = ET.SubElement(root, region)
        for w in weather[region]:
            weather_entry = ET.SubElement(r, "weather",
                                          {"weekday": w.weekday, "date": w.date.isoformat(" "), "title": w.title,
                                           "color": w.color})
            for info in w.information:
                ET.SubElement(weather_entry, "info").text = info

    xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="   ")
    with open(filename, "w") as f:
        f.write(xmlstr)

def fetchWeather(export_path):
    weather = {
        "germany": [],
        "north_alps": [],
        "south_alps": []
    }
    export_file = os.path.join(export_path, create_filename(datetime.datetime.utcnow()))
    if os.path.isfile(export_file):
        return readXML(export_file)

    result = requests.get(url)
    parsed_html = BeautifulSoup(result.text, features="html.parser")

    dhv_content = parsed_html.body.find('div', attrs={'id': 'dhv_content'})

    #states = ["date", "germany", "north_alps", "south_alps"]

    state = "date"
    for child in dhv_content:
        if state == "date" and type(child) is bs4.element.Tag and child.name == "p":
            date = child.text
            state = "germany"
        elif state in ["germany", "north_alps", "south_alps"] and type(child) is bs4.element.Tag and child.name == "div":
            if "class" not in child.attrs or len(child.attrs["class"]) == 0:
                continue
            if child.attrs["class"][0].startswith("wetter"):
                # weather element
                weather[state].append(parse_weather_tag(child))
            elif child.attrs["class"][0].startswith("csc-header"):
                for c in child:
                    if c.name == "h1" and c.text.strip("+").replace(" ", "") == "NORDALPEN":
                        state = "north_alps"
                    elif c.name == "h1" and c.text.strip("+").replace(" ", "") == "SÜDALPEN":
                        state = "south_alps"

    # example: 'Mo. 11.07.22 - 18h, nächste Aktualisierung Di. 8h.\r'
    temp = date.split("-")
    assert(len(temp) > 1)
    date = temp[0].strip()[4:]  # remove weekday
    date = datetime.datetime.strptime(date, "%d.%m.%y")

    time = temp[1].strip().split(",")
    assert (len(time) > 1)
    time = time[0]
    try:
        time = datetime.datetime.strptime(time, "%Hh")
    except ValueError:
        time = datetime.datetime.strptime(time, "%H.%Mh")

    shift = 0
    if time.hour > 12:
        shift = 1

    for key in weather:
        for i in range(len(weather[key])):
            weather_at_date = date + (i + shift) * datetime.timedelta(1)
            iso_weekday = german_weekday_to_iso(weather[key][i].title[0:2])
            assert(weather_at_date.isoweekday() == iso_weekday)  # recheck with weekday
            weather[key][i].setDate(weather_at_date)
            weather[key][i].weekday = weather[key][i].title[0:2]
            weather[key][i].title = weather[key][i].title[5:]

    writeXML(weather, export_path, export_file)

    return weather


if __name__ == "__main__":
    print(fetchWeather("export"))
