from markdown import markdown
from nio import UploadResponse
import aiofiles.os
from PIL import Image
import os.path


async def send_markdown(client, room_id, text):
    await client.room_send(
        # Watch out! If you join an old room you'll see lots of old messages
        room_id=room_id,
        message_type="m.room.message",
        content={"msgtype": "m.text", "format": "org.matrix.custom.html", "formatted_body": markdown(text), "body": ""},
        # for some reason an empty body must be provided
    )


async def send_text(client, room_id, text):
    await client.room_send(
        # Watch out! If you join an old room you'll see lots of old messages
        room_id=room_id,
        message_type="m.room.message",
        content={"msgtype": "m.text",
                 "mimetype": "text/plain",
                 "body": text},
        # for some reason an empty body must be provided
    )


async def send_code(client, room_id, code):
    if code == "":
        return
    await client.room_send(
        # Watch out! If you join an old room you'll see lots of old messages
        room_id=room_id,
        message_type="m.room.message",
        content={"msgtype": "m.text",
                 "mimetype": "text/plain",
                 "format": "org.matrix.custom.html",
                 "formatted_body": f"<pre><code>{code}</code></pre>",
                 "body": ""},

        # for some reason an empty body must be provided
    )


async def send_image(client, room_id, filename):
    """Send image to toom.

    Arguments:
    ---------
    client : Client
    room_id : str
    filename : str, file name of image

    This is a working example for a JPG image.
        "content": {
            "body": "someimage.jpg",
            "info": {
                "size": 5420,
                "mimetype": "image/jpeg",
                "thumbnail_info": {
                    "w": 100,
                    "h": 100,
                    "mimetype": "image/jpeg",
                    "size": 2106
                },
                "w": 100,
                "h": 100,
                "thumbnail_url": "mxc://example.com/SomeStrangeThumbnailUriKey"
            },
            "msgtype": "m.image",
            "url": "mxc://example.com/SomeStrangeUriKey"
        }

    """

    split = filename.rsplit('.', 1)
    #catches also empty strings
    if len(split) < 2:
        return "", ""

    filetype = split[1]

    svg = filetype == "svg"

    file_stat = await aiofiles.os.stat(filename)
    if not svg:
        content_type = f"image/{filetype}"
    else:
        content_type = "image/svg+xml"
    # first do an upload of image, then send URI of upload to room
    async with aiofiles.open(filename, "r+b") as f:
        resp, maybe_keys = await client.upload(
            f,
            content_type=content_type,
            filename=os.path.basename(filename),
            filesize=file_stat.st_size,
        )
    if isinstance(resp, UploadResponse):
        print("Image was uploaded successfully to server: " + filename)
    else:
        print(f"Failed to upload image. Failure response: {resp}")

    if not svg:
        im = Image.open(filename)
        (width, height) = im.size  # im.size returns (width,height) tuple
        content = {
            "body": os.path.basename(filename),  # descriptive title
            "info": {
                "size": file_stat.st_size,
                "mimetype": f"image/{filetype}",
                "thumbnail_info": None,  # TODO
                "w": width,  # width in pixel
                "h": height,  # height in pixel
                "thumbnail_url": None,  # TODO
            },
            "msgtype": "m.image",
            "url": resp.content_uri,
        }
    else:
        content = {
            "body": os.path.basename(filename),  # descriptive title
            "info": {
                "size": 773063, # file_stat.st_size,
                "mimetype": f"image/svg+xml",
                "thumbnail_info": {
                    "w": 800,
                    "h": 562,
                    "mimetype": "image/png",
                    "size": 193604
                },
                "w": 1491,  # width in pixel
                "h": 1047,  # height in pixel
                #"thumbnail_url": None,  # TODO
            },
            "msgtype": "m.image",
            "url": resp.content_uri,
        }
    try:
        await client.room_send(room_id, message_type="m.room.message", content=content)
        print("Image was sent successfully: " + filename)
    except Exception:
        print(f"Image send of file {filename} failed.")