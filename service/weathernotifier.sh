#!/usr/bin/env bash
# Needed for debian
### BEGIN INIT INFO
# Provides:          weathernotification
# Required-Start:    $all
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Weather notification service
### END INIT INFO

# Quick start-stop-daemon example, derived from Debian /etc/init.d/ssh
set -e

# Must be a valid filename
NAME=weathernotifier
PIDFILE=/var/run/$NAME.pid

# change fullpath
FULL_PATH="<path>"
DAEMON=./startWeathernotifier.sh

case "$1" in
  start)
        echo -n "Starting daemon: "$NAME
    start-stop-daemon --chdir $FULL_PATH --start --quiet --pidfile $PIDFILE --exec $DAEMON -- $DAEMON_OPTS
        echo "."
    ;;
  stop)
        echo -n "Stopping daemon: "$NAME
    start-stop-daemon --stop --quiet --oknodo --pidfile $PIDFILE
        echo "."
    ;;
  restart)
        echo -n "Restarting daemon: "$NAME
    start-stop-daemon --stop --quiet --oknodo --retry 30 --pidfile $PIDFILE
    start-stop-daemon --chdir $FULL_PATH --start --quiet --pidfile $PIDFILE --exec $DAEMON -- $DAEMON_OPTS
    echo "."
    ;;

  *)
    echo "Usage: "$1" {start|stop|restart}"
    exit 1
esac

exit 0
