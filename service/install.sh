#!/usr/bin/env bash

echo "installing weathernotifier service ..."

#POETRY_EXEC=$(which poetry)
PIPENV_EXEC=$(which pipenv)

#if [ -z "$POETRY_EXEC" ]
#then
  #curl -sSL https://install.python-poetry.org | python3 - 
  #POETRY_EXEC=$(which poetry)
  #echo "Poetry not found."
#fi

if [ -z "$PIPENV_EXEC" ]
then
  echo "Installing pipenv"
  pip install pipenv
  PIPENV_EXEC=$(which pipenv)
fi

if [ -z "$PIPENV_EXEC" ]
then
  echo "Pipenv not found: exit"
  exit 1
fi

echo "Pipenv executable: $PIPENV_EXEC"

CURR_PATH="$(dirname $(realpath "$0"))"
STARTSCRIPT=$(realpath "$CURR_PATH/..")

#echo -e "#!/usr/bin/env bash\n\n$POETRY_EXEC install\n$POETRY_EXEC run python weathernotifier/main.py" > "$CURR_PATH/../startWeathernotifier.sh"
echo -e "#!/usr/bin/bash\n$PIPENV_EXEC install\n$PIPENV_EXEC run python weathernotifier/main.py" > "$CURR_PATH/../startWeathernotifier.sh"
chmod +x "$STARTSCRIPT/startWeathernotifier.sh"

SUFFIX="_path"

sudo cp "$CURR_PATH/weathernotifier.sh" /etc/init.d/weathernotifier
sudo sed -i "s@<path>@$STARTSCRIPT@g" /etc/init.d/weathernotifier
sudo cp weathernotifier.service /etc/systemd/system
sudo sed -i "s@<username>@$USER@g" /etc/systemd/system/weathernotifier.service
sudo chmod +x /etc/init.d/weathernotifier

echo "created the weathernotifier service"

echo "Enabling service"
sudo systemctl enable weathernotifier

